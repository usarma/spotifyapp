const express = require('express');
const request = require("request");
const Async = require("async");
const jwt = require('jsonwebtoken');
const redis = require("redis"),
    client = redis.createClient();
const uuidv4 = require('uuid/v4');
const moment = require('moment');
const Helpers = require('../lib/helpers');

const router = express.Router();

const JWT_SECRET = 'Q$Bf@deCcM[mHz``#$@DE%awzAe!&]sSgq8/J!YJ;=V~&W5AcA"(C=?n:;.;';
const CLIENT_ID = '9e79c4187b464dc2be92f4c0dd2d81a6';
const CLIENT_SECRET = '2e57daa58fba4dc4b1a5c8584ba4d51e';

const REDIRECT_URI = process.env.ENV === "production" ? 'https://tunecroc.com/login_redirect' : 'http://localhost:3000/login_redirect';

const getAccessTokenRequest = (code) => {
    return {
        method: 'POST',
        url: 'https://accounts.spotify.com/api/token',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: {
            grant_type: 'authorization_code',
            code,
            redirect_uri: REDIRECT_URI,
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET
        }
    }
};

const fetchTracks = (user_id, track_ids, cb) => {
    if (track_ids.length === 0) {
        return cb(null, { tracks: [] })
    }

    database.collection('users').findOne({
        user_id
    }, (err, result) => {
        if (err) return cb(500);

        let array_of_tracks = [];
        const number_of_requests = Math.floor((track_ids.length - 1)/50) + 1;

        for (let i = 0; i < number_of_requests; i++){
            array_of_tracks.push([]);
        }

        for (let i = 0; i < track_ids.length; i++){
            array_of_tracks[Math.floor(i/50)].push(track_ids[i]);
        }

        Async.mapLimit(array_of_tracks, 2,
            (tracks, acb) => {
                const options = {
                    method: "GET",
                    url: "https://api.spotify.com/v1/tracks",
                    headers: {
                        'Authorization': 'Bearer ' + result.access_token
                    },
                    qs: {
                        ids: tracks.join(',')
                    }
                };

                request(options, (err, response, body) => {
                    if (err || response.statusCode !== 200) return acb(400);

                    acb(null, JSON.parse(body));
                });
            },
            (err, results) => {
                if (err) return cb(400);

                results = results.map(x => x.tracks).reduce((a, b) => {
                    return a.concat(b);
                }, []);

                cb(null, { tracks: results });
            });
    });
};

const refreshToken = (user_id, cb) => {
    database.collection('users').findOne({
        user_id
    }, (err, result) => {
        if (err) {
            return cb(500);
        }

        const options = {
            method: 'POST',
            url: 'https://accounts.spotify.com/api/token',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            form: {
                grant_type: 'refresh_token',
                refresh_token: result.refresh_token,
                client_id: CLIENT_ID,
                client_secret: CLIENT_SECRET
            }
        };

        request(options, (err, response, body) => {
            if (err || response.statusCode !== 200) {
                return cb(401);
            }

            body = JSON.parse(body);

            database.collection('users').updateOne({
                user_id: user_id
            }, {
                $set : {
                    access_token: body.access_token,
                    scope: body.scope,
                    access_token_expiry: new Date().getTime() + 3480000
                }
            }, (err) => {
                if(err) {
                    return cb(500);
                }

                cb(null, body);
            })
        });
    });
};

const getUserRequest = (access_token) => {
    return {
        method: 'GET',
        url: 'https://api.spotify.com/v1/me',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + access_token
        }
    }
};

const searchForSearchString = (params, access_token, country, cb) => {
    if (isNaN(params.offset)) {
        params.offset = 0;
    }

    if (!params.sstring || params.sstring.length < 3){
        return cb("Invalid search string");
    }

    params.offset = Math.floor(params.offset);
    params.offset = params.offset > 4 ? 4 : params.offset;

    const options = {
        method: 'GET',
        url: 'https://api.spotify.com/v1/search',
        headers: {
            'Authorization': 'Bearer ' + access_token
        },
        qs: {
            q: params.sstring + "*",
            type: 'album,artist,track',
            limit: 50,
            offset: params.offset,
            market: country
        }
    };

    request(options, (err, response, body) => {
        if (err || response.statusCode !== 200) {
            return cb(err || true, null);
        }

        body = JSON.parse(body);

        Object.keys(body).forEach(x => {
            Object.keys(body[x]).forEach(y => {
               if (y !== 'items') {
                   delete body[x][y];
               }
            });
        });

        if (body.albums && body.albums.items) {
            body.albums.items = body.albums.items.filter(x => x.album_type !== 'single');
        }

        if (body.tracks && body.tracks.items && body.tracks.items.length >= 0) {
            body.tracks.items.forEach(x => {
                client.set(x.id + "+" + country, true);
            });
        }

        if (body.albums && body.albums.items && body.albums.items.length >= 0) {
            body.albums.items.forEach(x => {
                client.set(x.id + "+" + country, true);
            });
        }

        if (body.artists && body.artists.items && body.artists.items.length >= 0) {
            body.artists.items.forEach(x => {
                client.set(x.id + "+" + country, true);
            });
        }

        cb(null, body);
    })
};

const searchForAlbum = (params, access_token, country, cb) => {
    if (!params.id) {
        return cb("Invalid album id");
    }

    const album_options = {
        method: 'GET',
        url: 'https://api.spotify.com/v1/albums/' + params.id,
        headers: {
            'Authorization': 'Bearer ' + access_token
        },
        qs : {
            market: country
        }
    };

    /*async.parallel({
        tracks: (acb) => {
            request(track_options, (err, response, body) => {
                if (err || response.statusCode !== 200) {
                    return acb("Unable to fetch tracks");
                }

                body = JSON.parse(body);

                Object.keys(body).forEach(x => {
                    if (x !== 'items') {
                        delete body[x];
                    }
                });

                acb(null, body);
            });
        },
        album: (acb) => {
            request(album_options, (err, response, body) => {
                if (err || response.statusCode !== 200) {
                    return acb("Unable to fetch album details");
                }

                body = JSON.parse(body);

                acb(null, body);
            });
        }
    }, (err, result) => {
        if (err) {
            return cb(err);
        }

        console.log(JSON.stringify(result));
        cb (null, {
            items: []
        });
    });*/

    request(album_options, (err, response, body) => {
        if (err || response.statusCode !== 200) {
            return cb("Unable to fetch album details");
        }

        body = JSON.parse(body);
        let tracks = JSON.parse(JSON.stringify(body.tracks));

        Object.keys(tracks).forEach(x => {
            if (x !== 'items') {
                delete tracks[x];
            }
        });

        delete body.tracks;

        tracks.items = tracks.items.map(x => {
           x.album = body;
           return x;
        });

        if (tracks.items && tracks.items.length >= 0) {
            tracks.items.forEach(x => {
                client.set(x.id + "+" + country, true);
            });
        }

        cb(null, { tracks: tracks, album: body });
    });
};

const searchForArtist = (params, access_token, country, cb) => {
    if (!params.id) {
        return cb("Invalid artist id");
    }

    // TODO: can fetch multiple albums at the same time. Do it. Or modify UI to select Artist >> Album >> Tracks

    const artist_options = {
        method: 'GET',
        url: 'https://api.spotify.com/v1/artists/' + params.id + '/albums',
        headers: {
            'Authorization': 'Bearer ' + access_token
        },
        qs: {
            limit: 50,
            market: country
        }
    };

    console.log(artist_options);

    request(artist_options, (err, response, body) => {
        if (err || response.statusCode !== 200) {
            return cb("Unable to fetch artist details");
        }

        body = JSON.parse(body);

        console.log(body);

        const ids = body.items.map(x => x.id);

        Async.mapLimit(ids, 6, (id, acb) => {
            searchForAlbum({ id: id }, access_token, country, acb);
        }, (err, result) => {
            if (err) {
                return cb("Unable to fetch artist details");
            }

            let tracks = {
                items: []
            };

            let albums = {
                items: []
            };

            result.forEach(x => {
                x.tracks.items.forEach(track => {
                    tracks.items.push(track);
                });

                albums.items.push(x.album);
            });

            if (tracks && tracks.items && tracks.items.length >= 0) {
                tracks.items.forEach(x => {
                    client.set(x.id + "+" + country, true);
                });
            }

            if (albums && albums.items && albums.items.length >= 0) {
                albums.items.forEach(x => {
                    client.set(x.id + "+" + country, true);
                });
            }

            cb(null, { tracks, albums });
        });
    });
};

const check_if_device_has_appeared = (params, cb) => {
    console.log('check_if_device_has_appeared', params);
    const devices = { method: 'GET',
        url: 'https://api.spotify.com/v1/me/player/devices',
        headers: {
            Authorization: "Bearer " + params.access_token
        }
    };

    let retries = 0;

    const find_if_device_apppeared = () => {

        console.log('find_if_device_apppeared', retries);

        if (retries >= 20) {
            return cb("Crossed max retries");
        }

        retries = retries + 1;

        request(devices, (err, response, body) => {
            if (err || response.statusCode !== 200) return cb(true);

            console.log('find_if_device_apppeared', body);

            body = JSON.parse(body);

            if (body && body.devices && body.devices.map(x => x.id).indexOf(params.device_id) >= 0) {
                return cb(null);
            }

            setTimeout(() => {
                find_if_device_apppeared();
            }, 300);
        });
    };

    find_if_device_apppeared();
};

const find_if_playback_started_on_correct_device = (params, cb) => {
    const current_playback = {
        method: "GET",
        url: "https://api.spotify.com/v1/me/player",
        headers: {
            'Authorization': 'Bearer ' + params.access_token
        }
    };

    console.log(current_playback);

    let retries = 0;

    const find_if_user_playback_started = () => {
        if (retries === 4) {
            return cb();
        }

        retries = retries + 1;

        request(current_playback, (err, response, body) => {

            console.log(err, response.statusCode, body);

            if (err || (response.statusCode !== 200 && response.statusCode !== 204)) {
                return cb(err || response.statusCode);
            }

            console.log('find_if_user_playback_started', body);

            // if (JSON.parse(body).device.id === params.device_id) {
            //     return cb(null);
            // }

            setTimeout(() => {
                find_if_user_playback_started();
            }, 1000);
        });
    };

    console.log('starting');

    find_if_user_playback_started();
};

const stop_current_playback = (params, cb) => {
    const current_playback = {
        method: "GET",
        url: "https://api.spotify.com/v1/me/player",
        headers: {
            'Authorization': 'Bearer ' + params.access_token
        }
    };

    const stop_playback = {
        method: "PUT",
        url: "https://api.spotify.com/v1/me/player/pause",
        headers: {
            'Authorization': 'Bearer ' + params.access_token
        }
    };

    let retries = 0;

    const find_if_user_has_current_playback = (acb) => {
        request(current_playback, (err, response, body) => {
            if (err || (response.statusCode !== 200 && response.statusCode !== 204)) {
                return cb(err || response.statusCode);
            }

            console.log('stop_current_playback', body);

            if (response.statusCode === 204) {
                return cb(null);
            }

            return acb(true);
        });
    };

    const find_if_user_has_current_stopped = () => {

        console.log('find_if_user_has_current_stopped', retries);

        if (retries >= 20) {
            return cb("Crossed max retries");
        }

        retries = retries + 1;

        request(current_playback, (err, response, body) => {
            if (err || (response.statusCode !== 200 && response.statusCode !== 204)) {
                return cb(err || response.statusCode);
            }

            if (response.statusCode === 200 && !body.is_playing) {
                return cb(null);
            }

            if (response.statusCode === 204) {
                return cb(null);
            }

            setTimeout(() => {
                find_if_user_has_current_stopped();
            }, 300);
        });
    };

    find_if_user_has_current_playback(() => {
        request(stop_playback, (err, response) => {
            console.log(err, response.statusCode);
            if (err || (response.statusCode !== 200 && response.statusCode !== 204)) {
                return cb(err || response.statusCode);
            }

            console.log('find_if_user_has_current_playback');

            find_if_user_has_current_stopped();
        })
    });
};

router.get('/', (req, res, next) => {
    res.render('index', {
        referrer : req.query.referrer
    });
});

router.get('/premium', (req, res, next) => {
    database.collection('users').findOne({ user_id : req.user_id }, (err, result) => {
        if (err || !result) return res.status(500).json(null);

        res.render('premium', {
            expiry : moment(result.premium_expiry).format('MMM D, YYYY'),
            referrer_code : result.referrer_code
        });
    });
});

router.get('/privacy', (req, res, next) => {
    res.render('privacy');
});

router.get('/user', (req, res, next) => {
    res.render('user');
});

router.get('/terms', (req, res, next) => {
    res.render('terms');
});

router.get('/about', (req, res, next) => {
    res.render('about');
});

router.get('/contact', (req, res, next) => {
    res.render('contact');
});

/* GET home page. */
router.get('/login', function(req, res, next) {
    const state = jwt.sign({ uuid: uuidv4(), referrer: req.query.referrer ? req.query.referrer : "none" }, JWT_SECRET);
    const scopes = 'user-read-private user-read-email user-read-playback-state streaming user-read-birthdate user-modify-playback-state user-library-read user-library-modify';

    res.redirect('https://accounts.spotify.com/authorize' +
        '?response_type=code' +
        '&client_id=' + CLIENT_ID +
        (scopes ? '&scope=' + encodeURIComponent(scopes) : '') +
        '&redirect_uri=' + encodeURIComponent(REDIRECT_URI) +
        '&state=' + state);
});

const decodeTheJwt = (req, res, next) => {
    try {
        const decoded = jwt.verify(req.query.state, JWT_SECRET);
        if (decoded.referrer !== "none") {
            req.referrer = decoded.referrer;
        }

        next();
    } catch (error) {
        res.redirect('/');
    }
};

const updateReferrerReferreeAndPremiumSubscriptionEndDates = (params, cb) => {
    if (params.referrer === 'none') return cb(false);

    Async.parallel({
        current_user : (acb) => {
            database.collection('users').findOne({ _id : params._id }, acb);
        },
        referrer_user : (acb) => {
            database.collection('users').findOne({ referrer_code : params.referrer }, acb);
        }
    }, (err, result) => {
        if (err || !result.referrer_user) console.log({ error : "Referral code doesn't exist" });
        if (err || !result.referrer_user) return cb({ error : "Referral code doesn't exist" });

        if (result.current_user.referrer_code === result.referrer_user.referrer_code) console.log({ error : "You cannot use your own referrer code" });
        if (result.current_user.referrer_code === result.referrer_user.referrer_code) return cb({ error : "You cannot use your own referrer code" });

        let update_current_user = {
            premium_expiry : new Date().getTime() + 1000*86400*30,
            referred_from : result.referrer_user._id
        };

        if (!!result.current_user.premium_expiry) {
            if (result.current_user.premium_expiry < new Date().getTime()) {
                update_current_user.premium_expiry = new Date().getTime() + 1000*86400*30;
            } else {
                update_current_user.premium_expiry = result.current_user.premium_expiry + 1000*86400*30;
            }
        } else {
            update_current_user.premium_expiry = new Date().getTime() + 1000*86400*30;
        }

        let update_referrer_user = {};

        if (!!result.referrer_user.premium_expiry) {
            // Max 6 months of premium
            const can_add_more_days =
                (!result.referrer_user.referred_from && result.referrer_user.availed_users.length < 3)
                || (result.referrer_user.referred_from && result.referrer_user.availed_users.length < 2) ? true : false;

            if (result.referrer_user.premium_expiry < new Date().getTime()) {
                if (can_add_more_days) {
                    update_referrer_user.premium_expiry = new Date().getTime() + 1000*86400*30;
                }
            } else {
                if (can_add_more_days) {
                    update_referrer_user.premium_expiry = result.referrer_user.premium_expiry + 1000*86400*30;
                }
            }
        } else {
            update_referrer_user.premium_expiry = new Date().getTime() + 1000*86400*30;
        }

        update_referrer_user.availed_users = result.referrer_user.availed_users.concat(params._id);

        console.log(update_current_user);
        console.log(update_referrer_user);

        Async.parallel({
            current_user_function : (acb) => {
                database.collection('users').updateOne({ _id : result.current_user._id }, {
                    $set : update_current_user
                }, acb);
            },
            refferer_user_function : (acb) => {
                database.collection('users').updateOne({ _id : result.referrer_user._id }, {
                    $set : update_referrer_user
                }, acb);
            }
        }, cb);
    });
};

const getAUnusedReferrerCode = async () => {
    return new Promise(async (resolve_top) => {
        const get_a_code = () => {
            const code = [];
            const alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1334567890";

            for (let i = 0; i < 7; i++) {
                code.push(alphabets[Math.floor(Math.random() * alphabets.length)]);
            }

            return code.join("");
        };

        let referrer_code = "";

        const find_if_code_has_been_used = async () => {
            referrer_code = get_a_code();
            return new Promise((resolve, reject) => {
                database.collection('users').findOne({ referrer_code: referrer_code }, (err, foundUser) => {
                    console.log(foundUser);
                    if (err || foundUser) {
                        return resolve(false);
                    }

                    return resolve(true);
                });
            });
        };

        while (true) {
            try {
                const code_not_found = await find_if_code_has_been_used();
                if (code_not_found) {
                    return resolve_top(referrer_code);
                }
            } catch (e) {

            }
        }
    });
};

router.get('/login_redirect', decodeTheJwt, function(req, res, next) {
    console.log(req.query);
    console.log("referrer", req.referrer);
    if (req.query.error || !req.query.code) {
        res.redirect('/');
        next();
        return;
    }

    request(getAccessTokenRequest(req.query.code), function (error, response, body) {
        if (error || response.statusCode !== 200) {
            return res.redirect('/');
        }

        body = JSON.parse(body);
        console.log(body);

        // Fetch user data for the access token, just emails and store.
        // Nothing is unique about the guy, one has to do this.
        request(getUserRequest(body.access_token), function (err, resp, result) {
            console.log(err);
            if (err || resp.statusCode !== 200) throw err;

            result = JSON.parse(result);

            console.log(result);

            if (result.product !== 'premium') {
                res.render('index', {
                    flash : "Sign up as a Spotify Premium User and try again."
                });
                return true;
            }

            body.access_token_expiry = new Date().getTime() + 3480000;
            body.code = req.query.code;
            body.user_id = result.uri;
            body.display_name = result.display_name;
            body.email = result.email;
            body.updated_at = new Date().getTime();
            body.country = result.country;

            database.collection('users').findOne({ user_id: result.uri }, async (err, foundUser) => {
                if (err) {
                    console.log("all manner of shit broke loose");
                    return res.render('index', {
                        flash : "Something went wrong! We are on it!"
                    });
                }

                console.log("gettng fudnUser");

                if (foundUser) {
                    return database.collection('users').updateOne({ user_id: result.uri }, {
                        $set : body
                    }, (err, updateOneResponse) => {
                        if (err) {
                            console.log("all manner of shit broke loose on update");
                            return res.render('index', {
                                flash : "Something went wrong! We are on it!"
                            });
                        }

                        res.cookie('auth', jwt.sign({ user_id: result.uri, expiry: body.access_token_expiry, country: body.country }, JWT_SECRET));
                        // One has to set a device name for each corresponding user.
                        return res.redirect('/play');
                    });
                }

                body.referrer_code = null;

                try {
                    body.referrer_code = await getAUnusedReferrerCode();
                    console.log(body.referrer_code);
                } catch (e) {

                }

                body.availed_users = [];
                body.referred_from = null;
                body.premium_expiry = new Date('2019-01-15').getTime();
                body.recurring = false;

                database.collection('users').insertOne(body, (err, insertResponse) => {
                    if (err) {
                        console.log("all manner of shit broke loose on insert");
                        return res.render('index', {
                            flash : "Something went wrong! We are on it!"
                        });
                    }

                    console.log('country_'+body.user_id, body.country, "votes_" + body.user_id);

                    Async.parallel([
                        (acb) => {
                            client.set('country_'+body.user_id, body.country, (err, result) => {
                                console.log(err, result);
                                return acb(err, result);
                            });
                        },
                        (acb) => {
                            client.set("votes_" + body.user_id, 10, (err, result) => {
                                console.log("is", err, result);
                                return acb(err, result);
                            });
                        }], (err) => {
                            if (err) {
                                console.log("all manner of shit broke loose on insert");
                                return res.render('index', {
                                    flash : "Something went wrong! We are on it!"
                                });
                            }

                            console.log("done with redis setting");

                            console.log(insertResponse);
                            const params = { referrer: req.referrer, _id : insertResponse.insertedId };

                            updateReferrerReferreeAndPremiumSubscriptionEndDates(params, (err) => {
                                res.cookie('auth', jwt.sign({ user_id: result.uri, expiry: body.access_token_expiry, country: body.country }, JWT_SECRET));
                                // One has to set a device name for each corresponding user.
                                return res.redirect('/play');
                            });
                        });
                });
            });
        });
    });
});

router.get('/play', (req, res, next) => {
    database.collection('users').findOne({
        user_id : req.user_id
    }, (err, result) => {
        if (err) throw err;

        // refresh token if it already expired or near expiry, make get user and refresh token if necessary method
        // instead of this shit
        console.log(result);
        res.render('play', {
            title: result.access_token,
            play: 'Play', username: result.display_name,
            referrer_code: result.referrer_code
        });
    });
});

router.get('/search', (req, res, next) => {
    // 1. fetch the user from somewhere
    // 2. search using his access key
    // 3. search that thing and send response.
    console.log(req.query);
    database.collection('users').findOne({
        user_id : req.user_id
    }, (err, result) => {
        if (err) throw err;

        if (!req.query.type) {
            searchForSearchString(req.query, result.access_token, result.country, (err, body) => {
                if (err) {
                    return res.status(500).json(null);
                }

                res.json(body);
            });
        } else if (req.query.type === "album") {
            searchForAlbum(req.query, result.access_token, result.country, (err, body) => {
                if (err) {
                    return res.status(500).json(null);
                }

                res.json({ tracks: body.tracks });
            });
        } else if (req.query.type === "artist") {
            searchForArtist(req.query, result.access_token, result.country, (err, body) => {
                if (err) {
                    return res.status(500).json(null);
                }

                res.json(body);
            });
        }
    });
});

router.post('/device', (req, res, next) => {
    console.log("device deteched", req.body);
    res.json({});
    // 1. This is called when we have to update device data once initialization is done on the client side.
    // 2. be cool
});

router.get('/auth_token', (req, res, next) => {
    database.collection('users').findOne({
        user_id : req.user_id
    }, (err, result) => {
        if (err) {
            return res.status(400).json(null);
        }

        res.json({
            name: uuidv4(),
            token: result.access_token
        });
    });
    // 1. This is called when we have to update device data once initialization is done on the client side.
    // 2. be cool
});

router.post('/refresh', (req, res, next) => {
    database.collection('users').findOne({
        user_id: req.user_id
    }, (err, result) => {
        if (err) {
            return res.status(500).json(null);
        }

        res.json({
            token: result.access_token
        })
    });
    // 1. refresh token entirely dependant on frontend
    // 2. be cool
});

router.get('/current_playlist', (req, res, next) => {
    client.get('top_picks+IN', (err, results) => {
        if (err) return res.status(500).json(null);

        if (!results) {
            results = {
                tracks: []
            };
        } else {
            results = JSON.parse(results);
        }

        return res.json({ tracks: results.tracks.slice(0, 10), counts: [] });
    });
});

router.get('/top_picks', (req, res, next) => {
    // database.collection('top_picks').find({
    //     expired: false,
    //     count: {
    //         $gt: 0
    //     }
    // }).sort({count: -1}).toArray((err, results) => {
    //     if (err) return res.status(500).json(null);
    //
    //     results = results.map(x => x.track);
    //     fetchTracks(req.user_id, results, (err, results) => {
    //         if (err) return res.status(err).json(null);
    //
    //         res.json(results);
    //     })
    // });

    client.get('top_picks+' + req.country, (err, results) => {
        if (err) return res.status(500).json(null);

        results = JSON.parse(results);

        if (!results) {
            results = {
                tracks: []
            };
        }

        const track_ids = results.tracks.map(x => x.id);
        database.collection('picks').find({
            user_id: req.user_id,
            track: {
                $in : track_ids
            },
            expired: false
        }).toArray((err, picks) => {
            if (err) res.status(500).json(null);

            let pick_counts = [];
            picks.forEach(x => {
                if (x.count > 0) {
                    pick_counts.push({
                        track: x.track,
                        count: x.count
                    });
                }
            });

            database.collection('users_queue').find({
                expired: false,
                user_id: req.user_id
            }).sort({ created_at: 1}).toArray((err, all_tracks_in_queue) => {
                 if (err || all_tracks_in_queue.length === 0) return res.json({ tracks: results.tracks, counts: pick_counts });

                 all_tracks_in_queue = all_tracks_in_queue.map(x => x.track);
                 fetchTracks(req.user_id, all_tracks_in_queue, (err, all_tracks_info) => {
                     if (err) return res.json({ tracks: results.tracks, counts: pick_counts });

                     return res.json({ tracks: results.tracks.slice(0, 1).concat(all_tracks_info.tracks).concat(results.tracks.slice(1)), counts: pick_counts });
                 });
            });
        });
    });
});

router.get('/picks', (req, res, next) => {
    console.log(req.query);
    database.collection('picks').find({
        user_id: req.user_id,
        expired: false,
        count: { $gt : 0 }
    }, { track: true, count: true }).toArray((err, results) => {
        if (err) return res.status(500).json(null);

        const track_ids = results.map(x => x.track);
        const track_counts = {};

        results.forEach(track => {
            track_counts[track.track] = track.count;
        });

        if (req.query.ids) {
            return res.json({ tracks: track_ids });
        }

        let time = new Date().getTime();
        fetchTracks(req.user_id, track_ids, (err, fetched_tracks) => {
            if (err) return res.status(err).json(null);

            console.log(new Date().getTime() - time);

            client.get('top_picks_counts+' + req.country, (err, popularity) => {

                if (err) return res.json(fetched_tracks);

                popularity = JSON.parse(popularity);

                if(!popularity) {
                    popularity = {};
                }

                fetched_tracks.tracks = fetched_tracks.tracks.map(track => {
                    track.popularity_status = popularity[track.id] ? "Hot" : null;
                    track.count = track_counts[track.id];
                    return track;
                });

                res.json(fetched_tracks);
            });
        });
    });
});

router.get('/previous_tracks', (req, res, next) => {
    database.collection('previously_played_tracks').find({
        country: req.country
    }, { track: true })
    .limit(50).sort({
        updated_at: -1
    }).toArray((err, results) => {
        if (err) return res.status(500).json(null);

        const track_ids = results.map(x => x.track);

        fetchTracks(req.user_id, track_ids, (err, fetched_tracks) => {
            if (err) return res.status(err).json(null);

            res.json(fetched_tracks);
        });
    });
});

const doesTrackExistForTheUser = (req, res, next) => {
    if (!req.body.track) {
        return res.status(400).json(null);
    }

    client.get(req.body.track + "+" + req.country, (err, result) => {
        if (err || !result) {
            return Helpers.tracksAndPlaylists.get_track_info({ user_id: req.user_id, id : req.body.track }, (err, track) => {
                if (err || !track) return res.status(400).json({
                    error : "Sorry! Track unavailable in your country even though its in your playlist!"
                });

                track = JSON.parse(track);

                if (track.available_markets.includes(req.country)) {
                    track.available_markets.forEach(x => {
                        console.log(req.body.track + "+" + x);
                        client.set(req.body.track + "+" + x, true);
                    });

                    return next();
                }

                return res.status(400).json({
                    error : "Sorry! Track unavailable in your country even though its in your playlist!"
                });
            });
        }

        next();
    });
};

router.post('/pick', doesTrackExistForTheUser, (req, res, next) => {
    console.log(req.body);
    if (!req.body.track) {
        return res.status(400).json(null);
    }

    console.log(req.body.track);

    // TODO : add counter here

    let increment = 1;

    if (req.body.remove) {
        increment = -1;
    }

    // update user usage table
    // check if track exists in db, top_picks
    // if not crate new one else update count of the track
    const update_database_insert = () => {
        database.collection('picks').findOne({
            user_id: req.user_id,
            track: req.body.track,
            expired: false
        }, (err, pick) => {
            if (err) return send_error(500);

            Async.parallel([
                //update picks
                (acb) => {
                    if (pick) {
                        database.collection('picks').updateOne({
                            _id: pick._id
                        }, { $inc: { count: 1 } }, (err) => {
                            if (err) return acb(500);

                            acb(null, pick.count + 1);
                        });
                    } else {
                        database.collection('picks').insertOne({
                            user_id : req.user_id,
                            track: req.body.track,
                            expired: false,
                            count: 1,
                            country: req.country
                        }, (err) => {
                            if (err) return acb(500);

                            acb(null, 1);
                        });
                    }
                },
                // update top_picks
                (acb) => {
                    database.collection('top_picks').findOne({
                        track: req.body.track,
                        country: {
                            $in : [ req.country ]
                        },
                        countries_pulled : {
                            $nin: [ req.country ]
                        }
                    }, (err, body) => {
                        if (err) {
                            return acb(500);
                        }

                        console.log(body);

                        if (body) {
                            database.collection('top_picks').updateOne({
                                _id: body._id
                            }, { $inc : { count: 1 }, $addToSet : { country : req.country } }, (err) => {
                                if (err) {
                                    return acb(500);
                                }

                                return acb();
                            });
                        } else {
                            database.collection('top_picks').insertOne({
                                track: req.body.track,
                                count: 1,
                                country: [req.country],
                                countries_pulled: []
                            }, (err) => {
                                if (err) {
                                    return acb(500);
                                }

                                return acb();
                            });
                        }
                    });
                },
                // update redis
                (acb) => {
                    acb();
                }
            ], (err, results) => {
                if (err) return send_error(err);

                client.get("votes_" + req.user_id, (err, result) => {
                    if (err) return res.status(err).json(null);

                    res.json({ votes: result, count: results[0] });
                });
            });
        });
    };

    const update_database_remove = () => {
        console.log("hi");
        Async.parallel([
            (acb) => {
                database.collection('picks').findOne({
                    user_id : req.user_id,
                    track: req.body.track,
                    expired: false,
                    count: { $gt: 0 }
                }, (err, pick) => {
                    if (err) return acb(500);
                    if (!pick) return acb(400);

                    acb(null, pick);
                });
            },
            (acb) => {
                database.collection('top_picks').findOne({
                    track: req.body.track,
                    country: {
                        $in : [ req.country ]
                    },
                    countries_pulled : {
                        $nin: [ req.country ]
                    },
                    count: { $gt : 0 }
                }, (err, top_pick) => {
                    if (err) return acb(500);
                    if (!top_pick) return acb(400);

                    console.log(top_pick);

                    return acb(null, top_pick);
                });
            }
        ], (err, result) => {
            if (err) return send_error(err);

            console.log(result[0]);

            Async.parallel([
                (acb) => {
                    database.collection('picks').updateOne({
                            _id: result[0]._id
                    }, { $inc: { count: -1 } }, (err) => {
                        if (err) return acb(500);

                        acb(null, result[0].count - 1);
                    });
                },
                (acb) => {
                    database.collection('top_picks').updateOne({
                        _id: result[1]._id
                    }, { $inc: { count: -1 } }, (err) => {
                        if (err) return acb(500);

                        acb();
                    });
                }
            ], (err, results) => {
                if (err) return send_error(err);

                client.get("votes_" + req.user_id, (error, result) => {
                    if (error) return send_error(500);

                    res.json({ votes: result, count: results[0]});
                });
            })
        });
    };

    // check if the track actually exists
    // cache already used tracks in redis
    // if not in redis, verify track and update database for user action

    const send_error = (status) => {
        client.get("votes_"+req.user_id, (err, votes) => {
            if (err) return res.status(status).json(null);

            votes = parseInt(votes);

            let next_number_of_votes = votes + increment;
            if (next_number_of_votes < 0 || next_number_of_votes > 10) {
                next_number_of_votes = votes;
            }

            client.set("votes_" + req.user_id, next_number_of_votes, () => {
                res.status(status).json(null);
            });
        });
    };

    client.get("votes_"+req.user_id, (err, votes) => {
        if(err) return res.status(500).json(null);

        votes = parseInt(votes);
        if (votes <= 0 && !req.body.remove) return res.status(400).json(null);

        let next_number_of_votes = votes - increment;
        if (next_number_of_votes < 0 || next_number_of_votes > 10) {
            next_number_of_votes = votes;
        }

        console.log("Number of votes remaining", next_number_of_votes);

        client.set("votes_"+req.user_id, next_number_of_votes, (err) => {
            if(err) return res.status(500).json(null);

            if (req.body.remove) {
                return update_database_remove();
            } else {
                return update_database_insert();
            }
        });
    });
});

router.get('/next_track', (req, res, next) => {
    Helpers.user.get_next_track({ user_id : req.user_id, dont_update : true, country: req.country }, (err, result) => {
        if (err) {
            return res.status(500).json(null);
        }

        result.track = "spotify:track:" + result.track;
        result.track_info = JSON.parse(result.track_info);
        result.picks = result.votes;

        res.json(result);
    });
});

router.post('/play_next', (req, res, next) => {
    if (!req.body.device_id) {
        return res.status(400).json(null);
    }

    Helpers.user.get_next_track({ user_id : req.user_id, country: req.country }, (err, result) => {
        if (err) {
            return res.status(500).json(null);
        }

        result.track = "spotify:track:" + result.track;
        result.track_info = JSON.parse(result.track_info);
        result.picks = result.votes;

        database.collection('users').findOne({
            user_id: req.user_id
        }, (err, user) => {
            if (err) return res.status(500).json(null);

            const options = { method: 'PUT',
                url: 'https://api.spotify.com/v1/me/player/play',
                qs: { device_id: req.body.device_id },
                headers:
                    {
                        'Content-Type': 'application/json',
                        Authorization: "Bearer " + user.access_token
                    },
                body: { uris: [ result.track ] },
                json: true };

            request(options, function (err, response, body) {
                if (err || (response.statusCode !== 200 && response.statusCode !== 204)) return res.status(500).json(null);

                res.json(result);
            });
        });
    });
});

router.post('/initialize_next', (req, res, next) => {

    /*strategy
    ask permission,
    find current playback,
    stop it,
    find whether it has stopped,
    start next playback. on this device.
    check whether next playback has play_bar_updater_started
    rinse repeat.
    * */

    console.log(req.body);

    database.collection('users').findOne({
        user_id: req.user_id
    }, (err, user) => {
        if (err) return res.status(500).json(null);

        Async.parallel({
            playback: (acb) => {
                console.log("playbacl");
                stop_current_playback(user, acb);
            },
            device: (acb) => {
                console.log("playbac2");
                check_if_device_has_appeared({
                    access_token: user.access_token,
                    device_id: req.body.device_id
                }, acb);
            }
        }, (err) => {
            if (err) return res.status(500).json(null);

            Helpers.user.get_next_track({ user_id : req.user_id, country: req.country }, (err, result) => {
                if (err) {
                    return res.status(500).json(null);
                }

                result.track = "spotify:track:" + result.track;
                result.track_info = JSON.parse(result.track_info);
                result.picks = result.votes;

                const options = { method: 'PUT',
                    url: 'https://api.spotify.com/v1/me/player/play',
                    qs: { device_id: req.body.device_id },
                    headers:
                        {
                            'Content-Type': 'application/json',
                            Authorization: "Bearer " + user.access_token
                        },
                    body: { uris: [ result.track ] },
                    json: true };

                request(options, function (err, response, body) {
                    if (err || (response.statusCode !== 200 && response.statusCode !== 204)) return res.status(500).json(null);

                    res.json(result);
                });
            });
        });
    });
});

router.get('/initialize_callback', (req, res, next) => {
    database.collection('users').findOne({
        user_id: req.user_id
    }, (err, user) => {
        if (err) {
            return res.status(500).json(null);
        }

        console.log(user);

        find_if_playback_started_on_correct_device(user, (err) => {
            console.log(err);
            res.json(null);
        });
    });
});

router.get('/saved_tracks', (req, res, next) => {
    Helpers.tracksAndPlaylists.get_user_saved_tracks(req.user_id, req.country, (err, result) => {
        if(err) {
            return res.status(500).json(null);
        }

        return res.json({
            tracks: result
        });
    });
});

const doesTrackExistForTheUserVarious = (req, res, next) => {
    if (!req.body.id) {
        return res.status(400).json(null);
    }

    client.get(req.body.id + "+" + req.country, (err, result) => {
        if (err || !result) {
            return Helpers.tracksAndPlaylists.get_track_info({ user_id: req.user_id, id : req.body.id }, (err, track) => {
                if (err || !track) return res.status(400).json({
                    error : "Sorry! Track unavailable in your country even though its in your playlist!"
                });

                track = JSON.parse(track);

                if (track.available_markets.includes(req.country)) {
                    track.available_markets.forEach(x => {
                        console.log(req.body.id + "+" + x);
                        client.set(req.body.id + "+" + x, true);
                    });

                    return next();
                }

                return res.status(400).json({
                    error : "Sorry! Track unavailable in your country even though its in your playlist!"
                });
            });
        }

        next();
    });
};

router.put('/play_now', doesTrackExistForTheUserVarious, (req, res, next) => {
    console.log(req.body);
    if (!req.body.device_id || !req.body.id) {
        return res.status(400).json(null);
    }

    Async.parallel({
        track_info : (acb) => {
            Helpers.tracksAndPlaylists.get_track_info({
                user_id: req.user_id,
                id: req.body.id
            }, acb)
        },
        votes: (acb) => {
            client.get('votes_' + req.user_id, (err, result) => {
                acb(null, result);
            });
        }
    }, (err, result) => {
        if (err) {
            return res.status(500).json(null);
        }

        result.track = "spotify:track:" + req.body.id;
        result.track_info = JSON.parse(result.track_info);
        result.picks = result.votes;

        database.collection('users').findOne({
            user_id: req.user_id
        }, (err, user) => {
            if (err) return res.status(500).json(null);

            const options = { method: 'PUT',
                url: 'https://api.spotify.com/v1/me/player/play',
                qs: { device_id: req.body.device_id },
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: "Bearer " + user.access_token
                },
                body: { uris: [ result.track ] },
                json: true };

            request(options, function (err, response, body) {
                if (err || (response.statusCode !== 200 && response.statusCode !== 204)) return res.status(500).json(null);

                res.json(result);
            });
        });
    });
});

router.post('/add_to_queue', doesTrackExistForTheUserVarious, (req, res, next) => {
    if(!req.body.id) {
        return res.status(400).json(null);
    }

    const track = {
        user_id : req.user_id,
        id : req.body.id,
        remove : req.body.remove
    };

    Helpers.tracksAndPlaylists.get_track_info(track, (err) => {
        if (err) return res.status(500).json(null);

        Helpers.user.add_to_users_queue(track, (err, result) => {
            if (err) return res.status(500).json(null);

            // Maybe send response of ones queue
            return res.status(200).json(null);
        })
    })
});

router.post('/save_track', doesTrackExistForTheUserVarious, (req, res, next) => {
    if(!req.body.id) {
        return res.status(400).json(null);
    }

    const track = {
        user_id : req.user_id,
        track : req.body.id
    };

    Helpers.tracksAndPlaylists.save_track_for_user(track, (err) => {
        if (err) return res.status(500).json(null);

        return res.status(200).json(null);
    })
});

router.get('/playlists', (req, res, next) => {
    client.get('user_playlists' + req.user_id, (err, results) => {
        if (results) {
            return res.json(JSON.parse(results));
        }

        Helpers.tracksAndPlaylists.get_user_playlists_and_tracks(req.user_id, req.country, (err, playlists) => {
            if (err) return res.status(500).json(null);

            client.set('user_playlists' + req.user_id, JSON.stringify(playlists), 'EX', 86400);
            res.json(playlists);
        });
    });
});

// TODO : switch to mysql maybe
// TOD : UI for picking off front page
// 1. color upon clicking and stuff
// TOD : cron
// TOD : for the top 5 picks, show the votes
// TOD : for the rest, just show hot
// TOD : for user picks, show which ones are in hot section
// TODO : add redis user votes counts
// TOD : create some way to down vote too
// TODO : keep space for play now and add to playlist things
// TODO : whatever

// TODO : steps to release
/*
Non Cosmetic

* show number of votes for top tracks
* show which tracks are hot in user's selections
*
* */

module.exports = router;

const request = require("request");
const Async = require("async");
const redis = require("redis"),
    client = redis.createClient();

const CLIENT_ID = '9e79c4187b464dc2be92f4c0dd2d81a6';
const CLIENT_SECRET = '2e57daa58fba4dc4b1a5c8584ba4d51e';

const get_paginated_objects = (request_object, cb) => {
    let paginated_objects = [];
    let total_paginated_objects = 0;

    request(request_object, (err, response, body) => {
        if (err || response.statusCode !== 200) {
            return cb(500);
        }

        body = JSON.parse(body);

        if (body.total <= body.items.length) {
            return cb(null, body.items);
        }

        paginated_objects = body.items;
        total_paginated_objects = body.total;

        Async.whilst(() => {
            return paginated_objects.length < total_paginated_objects;
        }, (acb) => {
            request_object.qs.offset = paginated_objects.length;
            request(request_object, (err, response, body) => {
                if (err || response.statusCode !== 200) {
                    return acb(500);
                }

                body = JSON.parse(body);

                paginated_objects = paginated_objects.concat(body.items);
            });
        }, (err) => {
            if (err) {
                console.log("fetching paginated_objects is fucked");
                return cb(err);
            }

            return cb(null, paginated_objects);
        });
    });
};

const user = {
    refresh_token_if_almost_expired : (user_id, cb) => {
        database.collection('users').findOne({
            user_id,
            access_token_expiry: {
                $lt : new Date().getTime()
            }
        }, (err, result) => {
            if (err) {
                return cb(500);
            }

            if (!result) return cb(null);

            const options = {
                method: 'POST',
                url: 'https://accounts.spotify.com/api/token',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                form: {
                    grant_type: 'refresh_token',
                    refresh_token: result.refresh_token,
                    client_id: CLIENT_ID,
                    client_secret: CLIENT_SECRET
                }
            };

            request(options, (err, response, body) => {
                if (err || response.statusCode !== 200) {
                    return cb(401);
                }

                body = JSON.parse(body);

                database.collection('users').updateOne({
                    user_id: user_id
                }, {
                    $set : {
                        access_token: body.access_token,
                        scope: body.scope,
                        access_token_expiry: new Date().getTime() + 3480000
                    }
                }, (err) => {
                    if(err) {
                        return cb(500);
                    }

                    cb(null, body);
                })
            });
        });
    },
    getUserAccess : (user_id, cb) => {
        database.collection('users').findOne({
            user_id : user_id
        }, cb);
    },
    add_to_users_queue : (params, cb) => {
        //this.getUserAccess(params.user_id, (err, result) => {
        database.collection('users_queue').update({
            user_id : params.user_id,
            track : params.id,
            expired : false
        }, {
            user_id : params.user_id,
            track : params.id,
            created_at : new Date().getTime(),
            expired : params.remove || false
        }, { upsert : true }, cb);
        //});
    },
    get_next_track : (params, cb) => {
        Async.parallel({
            next_track : (acb) => {
                database.collection('users_queue').find({
                    user_id : params.user_id,
                    expired : false
                }).sort({ created_at : 1 }).limit(1).toArray((err, tracks) => {
                    if (err) return acb(500);

                    if (tracks.length === 0)
                        return Async.parallel({
                            track : (acb) => {
                                client.get('current_track+' + params.country, acb);
                            },
                            track_info : (acb) => {
                                client.get('current_track_information+' + params.country, acb);
                            }
                        }, acb);

                    tracks = tracks[0];
                    params.id = tracks.track;
                    tracksAndPlaylists.get_track_info(params, (err, result) => {
                        if (err) return acb(500);

                        if (params.dont_update) {
                            return acb(null, { track: params.id, track_info: result });
                        }

                        database.collection('users_queue').updateOne({
                            _id : tracks._id
                        }, { $set : { expired : true }}, (err) => {
                            if (err) return acb(500);

                            return acb(null, { track: params.id, track_info: result });
                        });
                    });
                });
            },
            votes : (acb) => {
                client.get('votes_' + params.user_id, (err, result) => {
                    acb(null, result);
                });
            }
        }, (err, result) => {
            if (err) return cb(500);

            cb(null, {
                track : result.next_track.track,
                track_info : result.next_track.track_info,
                votes : result.votes
            });
        });
    },
};

const tracksAndPlaylists = {
    get_user_saved_tracks : (user_id, country, cb) => {
        user.getUserAccess(user_id, (err, result) => {
            if(err) {
                return cb(err);
            }

            let options = {
                method: "GET",
                url: "https://api.spotify.com/v1/me/tracks",
                headers: {
                    'Authorization': 'Bearer ' + result.access_token
                },
                qs: {
                    offset: 0,
                    limit: 50
                }
            };

            get_paginated_objects(options, (err, result) => {
                if (err)
                    return cb(err);

                console.log(result);

                result.forEach(x => {
                    x.track.available_markets.forEach(y => {
                        client.set(x.track.id + "+" + y, true);
                    });
                });

                return cb(null, result.map(x => x.track));
            });
        })
    },
    get_user_playlists : (user_id, cb) => {
        user.getUserAccess(user_id, (err, result) => {
            if(err) {
                return cb(err);
            }

            console.log(result);

            let options = {
                method: "GET",
                url: "https://api.spotify.com/v1/me/playlists",
                headers: {
                    'Authorization': 'Bearer ' + result.access_token
                },
                qs: {
                    offset: 0,
                    limit: 50
                }
            };

            get_paginated_objects(options, cb);
        });
    },
    // check_user_saved_tracks : (params, cb) => {
    //     if (params.tracks && params.tracks.length === 0) {
    //         return cb(null, {});
    //     }
    //
    //     user.getUserAccess(params.user_id, (err, result) => {
    //         if(err) {
    //             return cb(err);
    //         }
    //
    //         let options = {
    //             method: "GET",
    //             url: "https://api.spotify.com/v1/me/tracks/contains",
    //             headers: {
    //                 'Authorization': 'Bearer ' + result.access_token
    //             },
    //             qs: {
    //                 ids: params.tracks.join(',')
    //             }
    //         };
    //
    //         request(options, (err, response, body) => {
    //             if (err || response.statusCode !== 200) {
    //                 return cb(err);
    //             }
    //
    //             let tracks = {};
    //             body = JSON.parse(body);
    //
    //             params.tracks.forEach((x, index) => {
    //                 tracks[x] = body[index];
    //             });
    //
    //             return cb(null, tracks);
    //         });
    //     });
    // },
    save_track_for_user: (params, cb) => {
        user.getUserAccess(params.user_id, (err, result) => {
            if(err) {
                return cb(err);
            }

            let options = {
                method: 'PUT',
                url: 'https://api.spotify.com/v1/me/tracks',
                headers:
                    {
                        'Content-Type': 'application/json',
                        Authorization: "Bearer " + result.access_token
                    },
                body: { ids: [ params.track ] },
                json: true
            };

            request(options, (err, response) => {
                console.log(err, response.statusCode);
                if (err || response.statusCode !== 200) {
                    return cb(500);
                }

                return cb(null);
            });
        });
    },
    get_track_info : (params, cb) => {
        user.getUserAccess(params.user_id, (err, result) => {
            if(err) {
                return cb(err);
            }

            let options = { method: 'GET',
                url: 'https://api.spotify.com/v1/tracks/' + params.id,
                headers: {
                    Authorization: 'Bearer ' + result.access_token
                }
            };

            request(options, (err, response, body) => {
                if (err || response.statusCode !== 200) {
                    return cb(500);
                }

                return cb(null, body);
            });
        });
    },
    get_user_playlists_and_tracks : (user_id, country, cb) => {
        console.log(user_id);
        tracksAndPlaylists.get_user_playlists(user_id, (err, result) => {
            console.log(result.map(x => x.id));
            Async.mapLimit(result.map(x => x.id), 5, (playlist_id, acb) => {
                tracksAndPlaylists.get_tracks_for_playlist({
                    user_id: user_id,
                    id: playlist_id
                }, acb);
            }, (err, results) => {

                let playlists = [];

                result.forEach((playlist, index) => {
                    playlist = {
                        id : playlist.id,
                        name : playlist.name,
                        by : (playlist.owner && playlist.owner.display_name) ? playlist.owner.display_name : null,
                        tracks : results[index].map(x => x.track),
                    };

                    playlists.push(playlist);
                });

                playlists.forEach(playlist => {
                    playlist.tracks.forEach(track => {
                        if (track.available_markets.includes(country)) {
                            client.set(track.id + "+" + country, true);
                        }
                    });
                });

                cb(null, { playlists });
            });
        });
    },
    get_tracks_for_playlist : (params, cb) => {
        user.getUserAccess(params.user_id, (err, result) => {
            if (err) {
                return cb(err);
            }

            let options = {
                method: "GET",
                url: "https://api.spotify.com/v1/playlists/" + params.id + "/tracks",
                headers: {
                    'Authorization': 'Bearer ' + result.access_token
                },
                qs: {
                    offset: 0,
                    limit: 50
                }
            };

            get_paginated_objects(options, cb);
        });
    },
};

module.exports.user = user;
module.exports.tracksAndPlaylists = tracksAndPlaylists;

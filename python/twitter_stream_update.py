from twython import TwythonStreamer
from twython import Twython
import json


twitter = Twython('9JZFdhDUxtAVXzjGBlb87rYtt', 'B0XbeOynLPaAXCFTnv0iT9zTnrm1d9xhwZBC4TmOBJ8KdyLggp',
	'1066416273985294336-4RUvm2cDuxiTKp3ZMLUwuZO5EYDgpu', '8KRsDoowhKTkuwZCUhpSBHlVBcbcLw30gn8ODRa0Hgc9F')


def check_if_qualifies(data):
	if 'text' not in data.keys():
		return False

	if data['user']['lang'] != 'en':
		return False

	if "india" not in data['text'].lower():
		return False

	if "spotify" not in data['text'].lower():
		return False

	if int(data['user']['followers_count']) < 10 or int(data['user']['followers_count']) > 300:
		return False

	try:
		t = twitter.create_favorite(id=data['id'])
		print(json.dumps(t))
	except:
		print("Caught an exception trying to fav : " + data['id_str'])

	return True


class MyStreamer(TwythonStreamer):
    def on_success(self, data):
        if check_if_qualifies(data):
            print(json.dumps(data))
            print('\n')

    def on_error(self, status_code, data):
        print(status_code)

        # Want to stop trying to get data because of the error?
        # Uncomment the next line!
        # self.disconnect()

stream = MyStreamer('9JZFdhDUxtAVXzjGBlb87rYtt', 'B0XbeOynLPaAXCFTnv0iT9zTnrm1d9xhwZBC4TmOBJ8KdyLggp',
	'1066416273985294336-4RUvm2cDuxiTKp3ZMLUwuZO5EYDgpu', '8KRsDoowhKTkuwZCUhpSBHlVBcbcLw30gn8ODRa0Hgc9F')
stream.statuses.filter(track='#spotify,#india,#spotifyindia')

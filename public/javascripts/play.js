angular.module('PlayApp', ['rzModule', 'ngMaterial', 'ngMessages'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('[{');
        $interpolateProvider.endSymbol('}]');
    })
    .controller('PlayAppController', function($scope, $http, $window, $timeout, $mdDialog, $mdToast) {
        $scope.searchString = '';
        let search_time = 0;

        $scope.trackScores = {};
        $scope.trackTotalScores = {};
        $scope.showSearch = false;

        $scope.initalizing_message = "Initializing...";

        $scope.loading = true;
        $scope.tracks = [];
        $scope.artists = [];
        $scope.albums = [];

        const showToast = function(text, duration) {
            if(!text) {
                return false;
            }

            $mdToast.show($mdToast.simple().textContent(text).position("bottom right").hideDelay(duration || 3000));
        };

        $scope.showAdvanced = function(ev) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'user',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        function DialogController($scope, $mdDialog) {
            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function($event) {
                var copyText = angular.element( document.querySelector( '#referral-code' ) )[0];

                try {
                    var selection = window.getSelection();
                    var range = document.createRange();
                    range.selectNodeContents(copyText);
                    selection.removeAllRanges();
                    selection.addRange(range);

                    document.execCommand("copy");

                    if (window.getSelection) {window.getSelection().removeAllRanges();}
                    else if (document.selection) {document.selection.empty();}
                    showToast("Copied!");
                } catch (e) {
                    console.log("Something went wrong! Unable to copy!");
                }
            };
        }

        $scope.play_bar_updater_started = false;
        $scope.player_position = null;
        $scope.picksRemaining = '--';
        $scope.currentAlbumSearchId = "";
        $scope.currentArtistSearchId = "";

        $scope.pickedTracks = [];

        $scope.authToken = "";

        $scope.player_params = {};
        $scope.player = null;

        $scope.device_id = null;
        $scope.initializing = false;

        $scope.paused = true;
        $scope.state = {
            paused: true,
            duration: 0
        };

        $scope.first_call = true;
        $scope.called_for_next = false;
        $scope.playbackStarted = false;
        $scope.current_position = "00:00";
        $scope.track_length = "00:00";
        $scope.volume = 100;
        $scope.volumeSlider = {
            value: $scope.volume,
            options: {
                floor: 0,
                ceil: 100,
                showSelectionBar: true,
                onChange: function () {
                    $scope.volume = $scope.volumeSlider.value;
                    if ($scope.player) {
                        $scope.player.setVolume($scope.volume/100);
                    }
                }
            }
        };

        $scope.initProgress = {
            value: 25,
            options: {
                floor: 0,
                ceil: 100,
                showSelectionBar: true,
                readOnly: true
            }
        };

        $scope.trackProgress = 0;
        $scope.trackProgressSlider = {
            value: 0,
            options: {
                floor: 0,
                ceil: Math.floor($scope.state.duration/1000),
                showSelectionBar: true,
                readOnly: true
            }
        };

        $scope.stopTrackProgressUpdater = true;

        // Auto search disabled
        $scope.search = () => {
            if ($scope.searchString.length > 2) {
                const sstring = $scope.searchString;
                search_time = new Date().getTime();
                $timeout(() => {
                    if (new Date().getTime() - search_time >= 1000) {
                        $scope.doSearch(sstring);
                    }
                }, 1000);
            }
        };
        //$scope.search = () => {};

        $scope.loadingComplete = () => {
            return !$scope.loading;
        };

        $scope.hideSearch = ($event) => {
            $event.preventDefault();
            $event.stopPropagation();

            if ($event.target.className === "ng-scope") {
                $scope.showSearch = false;
            }
        };

        $scope.hideSearchByButton = () => {
            $scope.showSearch = false;
        };

        $scope.keyPressed = ($event) => {
            if ($event.key === "Escape") {
                $scope.showSearch = false;
            }
        };

        $scope.doSearch = (sstring, id, type) => {
            if ($scope.searchString.length < 3) {
                return false;
                // ping here
            }

            let url = 'search?sstring=' + $scope.searchString;

            if (id && type) {
                url = 'search?id=' + id + '&type=' + type;
            }

            $http({
                method: "GET",
                url: url
            }).then((result) => {
                $scope.tracks = result.data.tracks;
                if (!type || type !== "album") {
                    $scope.albums = result.data.albums;
                }

                if (!type || (type !== "album" && type !== "artist")) {
                    $scope.artists = result.data.artists;
                }

                if (type === "album" || !type) {
                    $scope.currentArtistSearchId = "";
                }

                if (type === "artist" || !type) {
                    $scope.currentAlbumSearchId = "";
                }

                $scope.showSearch = true;
            }).catch((err) => {
                console.log(err);
            });
        };

        $scope.searchForThisItem = (id, type) => {
            if (type === "album") {
                $scope.currentAlbumSearchId = id;
                $scope.doSearch("", id, type);
            } else if (type === "artist") {
                $scope.doSearch("", id, type);
                $scope.currentArtistSearchId = id;
            }
        };

        $scope.fetchOpacityAlbum = (id) => {
            if ($scope.currentAlbumSearchId === "") {
                return "false"
            }

            return id === $scope.currentAlbumSearchId ? "false" : "true";
        };

        $scope.fetchOpacityArtist = (id) => {
            if ($scope.currentArtistSearchId === "") {
                return "false"
            }

            return id === $scope.currentArtistSearchId ? "false" : "true";
        };

        $scope.songSelected = (id, pick, $event) => {
            if($event) {
                $event.preventDefault();
                $event.stopPropagation();
            }

            $http.post('/pick', { track: id, remove: !pick }).then((results) => {
                $scope.pickedTracks.push(id);
                $scope.picksRemaining = results.data.votes;
                $scope.trackScores[id] = results.data.count;
            }).catch((err) => {
                if (pick) {
                    showToast("You will get 10 more votes as next song is selected. In about 3 minutes. SIt back and enjoy the radio :)", 3000);
                }
            });
        };

        $scope.playNow = (id, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            $http.put('/play_now', { id: id, device_id: $scope.device_id }).then(result => {
                $scope.currentTrackId = result.data.track.split(':')[2];
                $scope.picksRemaining = result.data.picks;

                if ($scope.first_call) {
                    $scope.play_bar_updater();
                    $scope.stopTrackProgressUpdater = false;
                }

                $scope.currentTrackInfo = result.data.track_info;
                $scope.first_call = false;
                $scope.playbackStarted = true;
                $scope.current_position = "00:00";

                showToast("Playing!");
            }).catch(err => {
                console.log(err);
            });
        };

        $scope.remove = (track) => {
            return $scope.selectedItem === 'Playlist' && !track.popularity_status
        };

        $scope.addToQueue = (id, remove, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            $http.post('/add_to_queue', { id: id, remove }).then(result => {
                showToast(remove ? "Removed!" : "Added!", 1500);
            }).catch(err => {
                console.log(err);
            });
        };

        $scope.saveTrack = (id, $event) => {
            $event.preventDefault();
            $event.stopPropagation();

            $http.post('/save_track', { id: id }).then(result => {
                showToast("Saved!", 1000);
            }).catch(err => {
                console.log(err);
            });
        };

        const playback_has_been_paused = (state) => {
            return state && state.paused && state.position !== 0 && state.restrictions
            && state.restrictions.disallow_pausing_reasons
                && state.restrictions.disallow_pausing_reasons.length >= 1
            && state.restrictions.disallow_pausing_reasons[0] === "already_paused";
        };

        const playback_has_been_resumed = (state) => {
            return state && !state.paused && state.position !== 0 && state.restrictions
                && state.restrictions.disallow_resuming_reasons
                && state.restrictions.disallow_resuming_reasons.length >= 1
                && state.restrictions.disallow_resuming_reasons[0] === "not_paused";
        };

        $scope.trackClass = (id) => {
            return $scope.pickedTracks.includes(id) ? "track-picked" : "track-default";
        };

        $window.onSpotifyWebPlaybackSDKReady = () => {
            // You can now initialize Spotify.Player and use the SDK
            $scope.player = new Spotify.Player({
                name: "TuneCroc player",
                getOAuthToken: cb => {
                    $http.get('/auth_token')
                        .then(result => {
                            $scope.player_params = result.data;
                            $scope.initProgress.value = 50;
                            $scope.initalizing_message = "Connecting...";
                            cb(result.data.token);
                        });
                }
            });

            $scope.player.addListener('initialization_error', ( message ) => { console.log('initialization_error'); console.error(message);
                $scope.initalizing_message = "Failed to initialize Spotify Player, try another browser / try using Google Chrome.";
                $http.post('/device', {initialization_error: message});
            });
            $scope.player.addListener('authentication_error', ( message ) => { console.log('authentication_error'); console.error(message);
                $http.post('/device', {authentication_error: message});
            });
            $scope.player.addListener('account_error', ( message ) => { console.log('account_error'); console.error(message);
                $http.post('/device', {account_error: message});
            });
            $scope.player.addListener('playback_error', ( message ) => { console.log('playback_error'); console.error(message);
                $http.post('/device', {playback_error: message});
            });

            // Playback status updates
            $scope.player.addListener('player_state_changed', state => {
                if (!$scope.first_call) {
                    $scope.state = state;
                    $scope.initializing = false;
                    const duration = Math.round($scope.state.duration/1000);
                    const duration_minutes = Math.floor(duration/60) < 10 ? "0" + Math.floor(duration/60).toString() : Math.floor(duration/60).toString();
                    const duration_seconds = Math.floor(duration%60) < 10 ? "0" + Math.floor(duration%60).toString() : Math.floor(duration%60).toString();
                    $scope.track_length = duration_minutes + ":" + duration_seconds;
                    //$scope.trackProgressSlider.value = 100;

                    $scope.trackProgress = Math.round(state.position/1000);

                    $scope.trackProgressSlider = {
                        value: $scope.trackProgress,
                        options: {
                            floor: 0,
                            ceil: Math.floor($scope.state.duration/1000),
                            showSelectionBar: true,
                            readOnly: true
                        }
                    };

                    $scope.$digest();
                } else {
                    $scope.first_call = false;
                    $scope.player.setVolume($scope.volume/100);
                }

                //$http.post('/device', {state: JSON.stringify(state)});
                $scope.player_position = state.position;

                if (playback_has_been_paused(state)) {
                    $scope.stopTrackProgressUpdater = true;
                }

                if (playback_has_been_resumed(state)) {
                    $timeout(() => {
                        $scope.stopTrackProgressUpdater = false;
                        $scope.play_bar_updater();
                    }, 500);
                }

                if (state && state.paused && state.position === 0 && state.disallows && state.disallows.pausing ) {
                    $scope.playNext(err => {
                        console.log(err);
                    });
                    return false;
                }
            });

            // Ready
            $scope.player.addListener('ready', ({ device_id }) => {
                $scope.device_id = device_id;
                console.log('Ready with Device ID', device_id);
                $scope.initProgress.value = 75;
                $scope.initalizing_message = "Fetching playlist...";
                just_get_the_next_track(device_id, () => {
                    $scope.loading = false;
                    $timeout(() => {
                        $scope.$broadcast('reCalcViewDimensions');
                    }, 10);
                });

                $http.post('/device', { device_id: device_id });
            });

            // Not Ready
            $scope.player.addListener('not_ready', ({ device_id }) => {
                console.log('Device ID has gone offline', device_id);
                $http.post('/device', {not_ready: device_id});
            });

            // Connect to the player!
            $scope.player.connect();
            // Error handling

            $scope.fetch_current_top_tracks((err) => {
                if (err) console.log("Got error when fetching top tracks");

                update_token();
            });

            setTimeout(function(){
                $scope.$broadcast('reCalcViewDimensions');
            }, 10);
        };

        const update_token = () => {
            $timeout(() => {
                $http.post('/refresh').then(result => {
                    $scope.player_params.token = result.data.token;
                    update_token();
                }).catch(() => {
                    console.log("error updating auth token");
                });
            }, 2700000);
        };

        const just_get_the_next_track = (device_id, cb) => {
            $http.get('/next_track').then(result => {
                if (!$scope.first_call) {
                    return true;
                }

                if ($scope.currentTrackId && $scope.currentTrackId !== result.data.track.split(':')[2] && $scope.selectedItem === 'Playlist') {
                    $scope.fetch_current_top_tracks();
                }

                $scope.currentTrackId = result.data.track.split(':')[2];
                const currentTrackId = result.data.track;

                delete $scope.trackScores[$scope.currentTrackId];
                $scope.picksRemaining = result.data.picks;

                $http.post('/device', {
                    hello: "https://api.spotify.com/v1/me/player/play?device_id=" + device_id,
                    hellox: "https://api.spotify.com/v1/me/player/play?device_id=" + $scope.device_id,
                    starting:
                {
                    uris: [currentTrackId]
                },

                hi : {
                    headers: {
                        "Authorization" : "Bearer " + $scope.player_params.token
                    }}});

                $scope.currentTrackInfo = result.data.track_info;

                setTimeout(() => {
                    just_get_the_next_track(device_id, () => {});
                }, 2*60*1000);

                cb();
            }).catch(err => {
                console.log(err);
            });
        };

        $scope.playNext = () => {
            $http.post('/play_next', {
                device_id : $scope.device_id
            }).then(result => {
                $scope.currentTrackId = result.data.track.split(':')[2];
                delete $scope.trackScores[$scope.currentTrackId];
                $scope.picksRemaining = result.data.picks;

                $scope.currentTrackInfo = result.data.track_info;
            }).catch(err => {
                console.log(err);
            })
        };

        $scope.initializeNext = (cb) => {
            $http.post('/initialize_next', {
                device_id : $scope.device_id
            }).then(result => {
                $http.post('/device', {
                    start: "starting something"
                });

                $http.get('/initialize_callback', {
                    start: "starting something new"
                });

                $scope.currentTrackId = result.data.track.split(':')[2];
                delete $scope.trackScores[$scope.currentTrackId];
                $scope.picksRemaining = result.data.picks;

                $scope.currentTrackInfo = result.data.track_info;

                cb();
            }).catch(err => {
                $http.get('/initialize_callback', {
                    start: "starting something new1"
                });
                cb(err);
            });
        };

        $scope.top_list = [];
        $scope.selectedItem = 'Playlist';

        $scope.isSelected = (item) => {
            return $scope.selectedItem === item ? "picked" : "default";
        };

        $scope.playPause = () => {
            if (!$scope.playbackStarted) {
                showToast("Initializing...!", 3500);
                $scope.initializeNext(err => {
                    if (!err) {
                        $scope.playbackStarted = true;
                        $scope.stopTrackProgressUpdater = false;
                        $scope.play_bar_updater();
                    }
                });
            } else {
                $scope.player.togglePlay();
            }
        };

        $scope.fetch_current_top_tracks = (cb) => {
            $http.get('/top_picks').then(results => {
                $scope.selectedItem = 'Playlist';
                $scope.top_list = results.data.tracks;
                results.data.counts.forEach(x => {
                    $scope.trackScores[x.track] = x.count;
                });

                if ($scope.first_call) {
                    $http.get('/next_track').then(result => {
                        $scope.currentTrackId = result.data.track.split(':')[2];
                        delete $scope.trackScores[$scope.currentTrackId];
                        $scope.picksRemaining = result.data.picks;
                        $scope.currentTrackInfo = result.data.track_info;
                    }).catch(() => {});
                }

                if(cb) {
                    cb();
                }
            }).catch(err => {
                console.log(err);
                if(cb) {
                    cb();
                }
            })
        };

        $scope.fetch_user_picks = () => {
            $http.get('/picks').then(results => {
                $scope.selectedItem = 'My picks';
                $scope.top_list = results.data.tracks;
                $scope.top_list.forEach(x => {
                    $scope.trackScores[x.id] = x.count;
                });
            }).catch(err => {
                console.log(err);
            })
        };

        $scope.fetch_saved_tracks = () => {
            $http.get('/saved_tracks').then(results => {
                $scope.selectedItem = 'Saved tracks';
                $scope.top_list = results.data.tracks;
                $scope.top_list.forEach(x => {
                    $scope.trackScores[x.id] = x.count;
                });
            }).catch(err => {
                console.log(err);
            })
        };

        $scope.fetch_previously_played = () => {
            $http.get('/previous_tracks').then(results => {
                $scope.selectedItem = 'Previous tracks';
                $scope.top_list = results.data.tracks;
                $scope.top_list.forEach(x => {
                    $scope.trackScores[x.id] = x.count;
                });
            }).catch(err => {
                console.log(err);
            })
        };

        $scope.fetch_my_playlists = () => {
            $http.get('/playlists').then(results => {
                $scope.selectedItem = 'My playlists';
                $scope.playlists = results.data.playlists;
            }).catch(err => {
                console.log(err);
            })
        };

        $scope.play_bar_updater = () => {
            if ($scope.player) {
                $scope.player.getCurrentState().then(state => {
                    if (state && !$scope.stopTrackProgressUpdater) {
                        const pos = Math.round(state.position/1000);
                        $scope.trackProgressSlider.value = pos;
                        const pos_minutes = Math.floor(pos/60) < 10 ? "0" + Math.floor(pos/60).toString() : Math.floor(pos/60).toString();
                        const pos_seconds = Math.floor(pos%60) < 10 ? "0" + Math.floor(pos%60).toString() : Math.floor(pos%60).toString();
                        $scope.current_position = pos_minutes + ":" + pos_seconds;
                    }

                    if ($scope.stopTrackProgressUpdater) {
                        $scope.play_bar_updater_started = false;
                        return true;
                    }

                    $timeout(() => {
                        $scope.play_bar_updater();
                    }, 500);
                });
            }
        };
    });
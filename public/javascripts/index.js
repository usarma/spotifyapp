angular.module('PlayApp', ['ngMaterial', 'ngMessages'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[{');
    $interpolateProvider.endSymbol('}]');
}).controller('PlayAppController', function($scope, $http, $mdDialog) {
    $scope.top_list = [];
    $scope.selected_songs = [];

    const getTopTracks = () => {
        $http.get('/current_playlist').then(results => {
            $scope.top_list = results.data.tracks;
        }).catch(err => {
            console.log(err);
        })
    };

    $scope.songSelected = (id) => {
        if (!$scope.selected_songs.includes(id))
            $scope.selected_songs.push(id);
    };

    $scope.trackClass = (id) => {
        return $scope.selected_songs.includes(id) ? "track-picked" : "track-default";
    };

    getTopTracks();

    $scope.showAbout = function(ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'about',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })
            .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
    };

    $scope.showPrivacy = function(ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'privacy',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })
            .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
    };

    $scope.showContact = function(ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'contact',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })
            .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
    };

    $scope.showTerms = function(ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'terms',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })
            .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
    };

    function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function() {
            $mdDialog.hide();
        };
    }
});
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";
const redis = require("redis"),
    client = redis.createClient();
const async = require('async');
const request = require('request');

const CLIENT_ID = '9e79c4187b464dc2be92f4c0dd2d81a6';
const CLIENT_SECRET = '2e57daa58fba4dc4b1a5c8584ba4d51e';

const init = (cb) => {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        console.log("Cron initalizing database");
        const database = db.db('spotifyapp');
        cb(null, database);
    });
};

init((err, database) => {
    const refreshToken = (user_id, cb) => {
        database.collection('users').findOne({
            user_id
        }, (err, result) => {
            if (err) {
                return cb(500);
            }

            const options = {
                method: 'POST',
                url: 'https://accounts.spotify.com/api/token',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                form: {
                    grant_type: 'refresh_token',
                    refresh_token: result.refresh_token,
                    client_id: CLIENT_ID,
                    client_secret: CLIENT_SECRET
                }
            };

            request(options, (err, response, body) => {
                if (err || response.statusCode !== 200) {
                    return cb(401);
                }

                body = JSON.parse(body);

                database.collection('users').updateOne({
                    user_id: user_id
                }, {
                    $set : {
                        access_token: body.access_token,
                        scope: body.scope,
                        access_token_expiry: new Date().getTime() + 3480000
                    }
                }, (err, result) => {
                    if(err) {
                        return cb(500);
                    }

                    cb(null, body);
                })
            });
        });
    };

    const fetchTracks = (user_id, track_ids, cb) => {
        if (track_ids.length === 0) {
            return cb(null, { tracks: [] })
        }

        database.collection('users').findOne({
            user_id
        }, (err, result) => {
            if (err) return cb(500);

            let array_of_tracks = [];
            const number_of_requests = Math.floor(track_ids.length/50) + 1;

            for (let i = 0; i < number_of_requests; i++){
                array_of_tracks.push([]);
            }

            for (let i = 0; i < track_ids.length; i++){
                array_of_tracks[Math.floor(i/50)].push(track_ids[i]);
            }

            async.mapLimit(array_of_tracks, 2,
                (tracks, acb) => {
                    const options = {
                        method: "GET",
                        url: "https://api.spotify.com/v1/tracks",
                        headers: {
                            'Authorization': 'Bearer ' + result.access_token
                        },
                        qs: {
                            ids: tracks.join(',')
                        }
                    };

                    request(options, (err, response, body) => {
                        if (err || response.statusCode !== 200) return acb(400);

                        acb(null, JSON.parse(body));
                    });
                },
                (err, results) => {
                    if (err) return cb(400);

                    results = results.map(x => x.tracks).reduce((a, b) => {
                        return a.concat(b);
                    }, []);

                    cb(null, { tracks: results });
                });
        });
    };

    const fetchTopTrack = (cb) => {
        database.collection('top_picks').find({
            count: {
                $gt : 0
            }
        }).sort({count: -1}).limit(50).toArray((err, top_tracks) => {
            if (err || top_tracks.length === 0) {
                console.log("top tracks are empty, no cry", err);

                database.collection('previously_played_tracks').find().limit(50).toArray((err, top_tracks) => {
                    if (err || top_tracks.length === 0) {
                        console.log("Yousa really really fucked, now cry", err);
                    }

                    return cb(null, {
                        top_tracks: [top_tracks[Math.floor((Math.random() * top_tracks.length))]],
                        from_previous_tracks: true
                    });
                });

                return false;
            }

            return cb(null, { top_tracks: top_tracks});
        });
    };

    const fetchARefreshedUser = (cb) => {
        const current_time = new Date().getTime();
        database.collection('users').findOne({
            access_token_expiry: {
                $gt: current_time + 120000
            }
        }, (err, user) => {
            if (err || !user) {
                database.collection('users').find().limit(10).toArray((err, users) => {
                    if (err) {
                        throw new Error();
                    }

                    user = users[Math.floor((Math.random() * users.length))];
                    refreshToken(user.user_id, () => {
                        database.collection('users').findOne({
                            access_token_expiry: {
                                $gt: current_time + 120000
                            }
                        }, cb);
                    })
                });

                return false;
            }

            return cb(err, user);
        })
    };

    const cron = () => {
        const current_time = new Date().getTime();

        console.log("Running cron");

        fetchARefreshedUser((err, user) => {
            if (err) {
                console.log("Cron died, now cry", err);
                return false;
            }

            fetchTopTrack((err, the_top_track) => {
                if (err) {
                    console.log("This shouldn't be happening, error fetching top track after modification, really cry", err);
                    return refreshToken(user.user_id, () => {
                        setTimeout(() => {
                            cron();
                        }, 120000);
                    })
                }

                const top_track = the_top_track.top_tracks[0];
                console.log(top_track);
                console.log(user);

                async.parallel([
                    (acb) => {
                        client.set('current_track', top_track.track, (err) => {
                            if (err) {
                                console.log("Redis died, fuck you, now cry", err);
                            }

                            console.log("Set current track on redis");
                            return acb(err);
                        });
                    },
                    (acb) => {
                        database.collection('top_picks').update({
                            _id: top_track._id
                        }, { $set : { expired: true, updated_at: current_time } }, (err, result) => {
                            if (err) {
                                console.log("Fucked up, now cry", err);
                            }

                            console.log("Updated top_picks, removed top track from top_picks");
                            acb(err);
                        })

                    },
                    (acb) => {
                        database.collection('picks').update({
                            track: top_track.track,
                            expired: false,
                            removed: {
                                $ne: true
                            }
                        }, { $set : { expired: true, updated_at: current_time } }, {multi: true}, (err, result) => {
                            if (err) {
                                console.log("Fucked up, now cry", err);
                            }

                            console.log(result);

                            console.log("Updated picks, expired track from all");
                            acb(err);
                        });
                    },
                    (acb) => {
                        if (the_top_track.from_previous_tracks) {
                            return acb();
                        }

                        database.collection('previously_played_tracks').insertOne({
                            track: top_track.track,
                            count: top_track.count,
                            updated_at: current_time
                        }, (err, result) => {
                            if (err) {
                                console.log("Kinda useless, no cry", err);
                            }

                            console.log(result);

                            console.log("updated previous tracks list");
                            acb();
                        });
                    },
                    (acb) => {
                        console.log(user);
                        refreshToken(user.user_id, () => {
                            acb();
                        })
                    },
                    (acb) => {
                        database.collection('users').find({}).toArray((err, users) => {
                            console.log();
                            if (err) console.log("Couldn't fetch all users, do cry", err);

                            users = users.map(x => x.user_id);
                            async.eachLimit(users, 30, (user_id, bcb) => {
                                 client.set("votes_" + user_id, 10, (err) => {
                                     if (err)
                                        console.log("Couldnt set a user's votes, no cry", err);

                                    bcb(err);
                                 })
                            }, acb);
                        })
                    },
                    (acb) => {
                        const options = {
                            method: "GET",
                            url: "https://api.spotify.com/v1/tracks/" + top_track.track,
                            headers: {
                                "Authorization" : "Bearer " + user.access_token
                            }
                        };

                        request(options, (err, response, body) => {
                            console.log(err, response.statusCode);
                            if (err || response.statusCode !== 200) {
                                console.log("fetch track died, now cry");
                                return acb(err || response.statusCode);
                            }

                            console.log("Fetched top track information");

                            const track_information = JSON.parse(body);
                            const refresh_after = track_information.duration_ms - 15000;

                            console.log("refresh track after", refresh_after);
                            setTimeout(() => {
                                cron()
                            }, refresh_after);

                            client.set('cron_refresh_at', current_time + refresh_after, (err) => {
                                if (err) {
                                    console.log("couldnt set refresh time, now cry");
                                }

                                client.set('current_track_information', body, (err) => {
                                    if (err) {
                                        console.log("Save the track information dies, now cry");
                                    }

                                    console.log("saved track information");

                                    acb(err);
                                });
                            });
                        });
                    }
                ], (err, result) => {
                    if (err) {
                        console.log(err);
                    }

                    let results = the_top_track.top_tracks;

                    const tracksids = results.map(x => x.track);
                    const counts = results.map(x => x.count);
                    fetchTracks(user.user_id, tracksids, (err, results) => {
                        if (err) console.log(err);

                        const top_picks_counts = {};
                        results.tracks = results.tracks.map((track, index) => {
                            if (index === 0) {
                                track.popularity_status = "Now Playing"
                            } else {
                                track.popularity_status = index < 5 ? counts[index].toString() : "Hot";
                                top_picks_counts[track.id] = track.popularity_status;
                            }

                            return track;
                        });

                        client.set('top_picks', JSON.stringify(results));
                        client.set('top_picks_counts', JSON.stringify(top_picks_counts));
                    });

                    console.log(result);
                });
            });
        });
    };

    client.get('cron_refresh_at', (err, result) => {
        const current_time = new Date().getTime();
        if (err) {
            console.log("Yousa fucked");
        }

        if (!result) {
            return cron();
        }

        console.log("Will run after", parseInt(result) - current_time, "ms");

        setTimeout(() => {
            cron()
        }, parseInt(result) - current_time);
    });
});
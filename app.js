var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var url  = require('url');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var Helper = require('./lib/helpers');

var app = express();

app.disable('etag');
const jwt = require('jsonwebtoken');
const jwt_secret = 'Q$Bf@deCcM[mHz``#$@DE%awzAe!&]sSgq8/J!YJ;=V~&W5AcA"(C=?n:;.;';
database = null;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
    // Send everything somewhere
    var url_parts = url.parse(req.url);
    console.log(url_parts.pathname);
    if (url_parts.pathname !== '/'
        && url_parts.pathname !== '/login'
        && url_parts.pathname !== '/login_redirect'
        && url_parts.pathname !== '/current_playlist'
        && url_parts.pathname !== '/terms'
        && url_parts.pathname !== '/contact'
        && url_parts.pathname !== '/about'
        && url_parts.pathname !== '/privacy') {
        try {
            const decoded = jwt.verify(req.cookies.auth, jwt_secret);
            req.user_id = decoded.user_id;
            req.country = decoded.country;

            if (!req.user_id) {
                res.redirect('/');
                return;
            }

            if (new Date().getTime() < decoded.expiry) {
                return next();
            }

            Helper.user.refresh_token_if_almost_expired(req.user_id, () => {
                res.cookie('auth', jwt.sign({ user_id: req.user_id, expiry: new Date().getTime() + 3480000, country: req.country }, jwt_secret));
                next();
            });
        } catch (e) {
            res.redirect('/');
        }
    } else {
        next();
    }
});

app.use((req, res, next) => {
    const url_parts = url.parse(req.url);
    database.collection('logs').insertOne({
        url : url_parts.pathname,
        user_id : req.user_id,
        query : req.query,
        body : req.body,
        method : req.method,
        created_at : new Date().getTime()
    });
    next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
